import { Component, OnInit } from '@angular/core';
import { Produit } from '../models';
import {ProduitServiceService } from '../produit-service.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-produit',
  template: `
    <style>input.ng-invalid {border: 3px blue solid;}input.ng-invalid.ng-dirty {border: 3px blue solid;}</style>
    <table border="1">
      <tr>
        <td>Nom</td>
        <td>Description</td>
        <td>Ingredients</td>
        <td>Prix</td>
        <td>Supprimer</td>
      </tr>
      <tr *ngFor="let produit of ListeProduits; even as isEven" [style.background-color]="isEven ? '#97A3A1' : '#C1ECE6'">
        <td>{{produit.nom | uppercase}}</td>
        <td>{{produit.description | lowercase}}</td>
        <td>{{produit.listeIngredients}}</td>
        <td>{{produit.prix | number}} €</td>
        <td><button (click)="supprimer(produit)">X</button></td>
      </tr>
    </table>

    <button (click)="afficherForm()">Ajouter un produit</button>

    <div [hidden]="isHidden">
      <h2>Nouveau Produit</h2>
      <form (ngSubmit)="register()" [formGroup]="produitForm">
        <div>
          <label>Nom</label>
          <input formControlName="nom">
          <div *ngIf="nomCtrl.dirty && nomCtrl.hasError('required')">Le champ nom est requis</div>
        </div>
        <div>
          <label>Prenom</label>
          <input formControlName="fournisseur">
          <div *ngIf="fournisseurCtrl.dirty && fournisseurCtrl.hasError('required')">Le champ prénom est requis</div>
        </div>
        <div>
          <label>Age</label>
          <input formControlName="age">
          <div *ngIf="ageCtrl.dirty && ageCtrl.hasError('required')">Le champ âge est requis</div>
        </div>
        <div>
          <label>Nom Classe</label>
          <input formControlName="description">
          <div *ngIf="descriptionCtrl.dirty && descriptionCtrl.hasError('required')">Le champ âge est requis</div>
        </div>
        <button type="submit" [disabled]="produitForm.invalid" >Enregistrer</button>
      </form>
    </div>
  `,
  styleUrls: ['./produit.component.css']
})


export class ProduitComponent implements OnInit {

  nomCtrl: FormControl;
  fournisseurCtrl: FormControl;
  ageCtrl: FormControl;
  descriptionCtrl: FormControl;
  produitForm: FormGroup;
  ListeProduits: Array<Produit>;
  isHidden : boolean;

  constructor(private ProduitsService: ProduitServiceService, fb: FormBuilder) {
    this.isHidden = true;
    this.nomCtrl = fb.control('', Validators.required);
    this.fournisseurCtrl = fb.control('', Validators.required);
    this.descriptionCtrl = fb.control('', Validators.required);
    this.ageCtrl = fb.control('', Validators.required);
    this.produitForm = fb.group({
      nom: this.nomCtrl,
      fournisseur: this.fournisseurCtrl,
      description: this.descriptionCtrl,
      age: this.ageCtrl,
    });
  }

  supprimer(produit){
    console.log(this.ListeProduits);
    this.ListeProduits = this.ListeProduits.filter(produitListe => produitListe.nom != produit.nom);
    console.log(this.ListeProduits);
  }

  register() {
    this.ProduitsService.add(this.produitForm.value);
  }

  afficherForm(){
    this.isHidden = false;
  }

  ngOnInit(): void {
    this.ListeProduits = this.ProduitsService.list();
  }

}
