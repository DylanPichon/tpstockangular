export  class Produit {
  nom: string;
  fournisseur: string;
  emailFournisseur: string;
  listeIngredients: Array<string>;
  description: string;
  age: number;
  conditionConservation: string;
  prix: number;

  constructor(nom, fournisseur, description, age, conditionConservation = "", prix = null, emailFournisseur="", listeIngredients=[]) {
    this.nom = nom;
    this.fournisseur = fournisseur;
    this.emailFournisseur = emailFournisseur;
    this.listeIngredients = listeIngredients;
    this.description = description;
    this.age = age;
    this.conditionConservation = conditionConservation;
    this.prix = prix;
  }
}

export const exemplesProduits = [
  new Produit('Voiture', 'Renault', 'Roule vite',1,'Entretenir',12000,'Renault@email.fr', ['Roue', 'Carosserie', 'Pneu']),
  new Produit('Camion', 'Mercedes', 'Roule lentement',2, 'Beaucoup de place', 24000,'Mercedes@email.fr', ['Roue', 'Carosserie', 'Pneu', 'Klaxon']),
  new Produit('Avion', 'AirFrance', 'vole', 3, 'Piste atterissage', 6585, 'AirFrance.email.fr', ['Ailes', 'Carosserie', 'Roues']),
  new Produit('Train', 'SNCF', 'Roule vite sur des rails',4,'Gare',66666666,'SNCF@email.fr', ['Rail', 'Carosserie', 'Klaxon']),
  new Produit('Bateau', 'FranceFeries', 'Flotte',5, 'Port',333333,'FranceFeries@email.fr', ['Proue', 'Gouvernail', 'Voila']),
];
