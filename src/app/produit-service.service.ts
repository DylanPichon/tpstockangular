import { Injectable } from '@angular/core';
import { Produit, exemplesProduits} from './models';

@Injectable({
  providedIn: 'root'
})
export class ProduitServiceService {
  listeProduits: Array<Produit> = exemplesProduits;

  constructor() { }

  list(){
    return this.listeProduits;
  }

  add(produit: Produit){
    this.listeProduits.push(produit);
  }
}
